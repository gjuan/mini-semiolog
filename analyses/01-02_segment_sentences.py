#%% [markdown]
# # Segmenting Sentences
#%% [markdown]
# ## Initialize
#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *
#%%

# Load vocabulary and sentences to segment

voc_A = slg.load_vocabulary(
    params.vocFileName
)


#%%


sents = slg.load_test_sents(
    params.inputSentences
)

# Segment sentences into sequences (seg_type = "seq") or trees (seg_type = "tree"), and save them

segs = slg.segment_chainlist(
    sents,
    voc_A,
    seg_type = params.segType,
    sample_size = params.sampleSize,
    randomQ = params.randomSegQ,
    saveQ = params.saveSegQ,
    filename = params.segFileName,
    )

#%%
# plot a random tree

# import random
# slg.plot_tree(random.choice(segs))
# %%