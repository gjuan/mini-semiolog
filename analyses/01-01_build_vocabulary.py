#%% [markdown]
# # Building Vocabulary

#%% [markdown]
# ## Initialize

#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

#%%
# Load Corpus

chain = slg.load_corpus(
    filename = params.inputCorpus,
    length = params.corpusPartLength
)
#%%
voc = slg.build_vocabulary(
    chain,
    printQ = params.vocBuildPrintQ,
    parallelQ = params.parallelVoc,
    n_cores = params.nCores,
    voc_len = params.vocLen,
    inter_results = params.interResults,
    resumeQ = params.resumeQ,
    save_finalQ = params.saveFinalQ,
    filename = params.vocFileName,
    corpus_name = params.inputCorpus,
    courpus_length = params.corpusPartLength,
)


# %%