#%% [markdown]
# # Plotting Types

#%% [markdown]
# ## Modules and Parameters
#%%
# INITIALIZE
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

# %% [markdown]
### Load Type Lists

#%%
filename = params.typesFileName

boRoR_T, boLoL_T = slg.load_type_list(
    filename
    )


# %% [markdown]
### Build Graph Relations

boRoR_graph = slg.build_ortho_graph(
    boRoR_T,
    side = "R",
    ortho = True,
    subtype_filter = params.subtypeFilter,
    filter_sT = params.subtypeReduction,
    )
boLoL_graph = slg.build_ortho_graph(
    boLoL_T,
    side = "L",
    ortho = True,
    subtype_filter = params.subtypeFilter,
    filter_sT = params.subtypeReduction,
    )

complete_graph = slg.compose_graphs([boLoL_graph, boRoR_graph])

# %% [markdown]
### Plot Graph
# %%

lattice_graph = slg.lattice_graph(
    # boLoL_graph,
    # boRoR_graph,
    complete_graph,
    fname = paths.lattice_graphs+filename,
    dependenciesQ = True,
)
lattice_graph.view()

# %%