#%% [markdown]
# # Computing biorthogonal closure

#%% [markdown]
# ## Modules and Parameters
#%%
# INITIALIZE
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *


#%% [markdown]
# ## CODE
#%% [markdown]
# ## Load Data
# ### Orthogonals

orthogonals = slg.load_orthogonality_data(
    filename = params.orthoFileName,
    freq_min = params.freqMinOrthoFilter,
)

#%% [markdown]
# ### Terms and contexts

#%%

terms, contexts = slg.build_terms_contexts(
    orthogonals,
    hand_picked_terms = params.hpTerms,
    hand_picked_contexts = params.hpContexts,
    trim_terms = params.trimTerms,
    trim_contexts = params.trimContexts,
    symmetricQ = params.symmetricQ,
)

#%% [markdown]
# ## Matrices

# ### Term-Context Matrix

#%%
oR_M = slg.build_term_context_matrix(
    terms,
    contexts,
    orthogonals,
    normalizeQ=True)

oL_M = oR_M.T
#%% [markdown]

# ### PMI

#%%

print("PMI R...")
oR_M_pmi = slg.build_pmi_matrix(
    oR_M,
    type=params.typePMI,
    alpha= params.alpha,
    normalizeQ = True,
    )

print("PMI L...")
oL_M_pmi = slg.build_pmi_matrix(
    oL_M,
    alpha= params.alpha,
    type=params.typePMI,
    normalizeQ = True,
    )

#%% [markdown]

# ### Binary
#%%

oR_M_bin, ooR_M_bin = [
    slg.build_bin_matrix(
        matrix,
        cut_func = params.cutFunc,
        thres=params.thres, # mean(oR_M_pmi.data)*1.25,
        )
    for matrix in [oR_M_pmi, oR_M_pmi.T]
]


oL_M_bin, ooL_M_bin = [
    slg.build_bin_matrix(
        matrix,
        cut_func = params.cutFunc,
        thres=params.thres, # mean(oR_M_pmi.data)*1.25,
        )
    for matrix in [oL_M_pmi, oL_M_pmi.T]
    ]

#%% [markdown]
# ## Compute Closure

#%%

oR_M_ps, oL_M_ps = slg.powerset_M_parallel(
    [oR_M_bin, oL_M_bin],
    reduce_thres = params.reduceThres,
    bound = 20,
)

#%% [markdown]
# ## Build Types from PowerSet
# ### Biorthogonal Matrices
#%%

boR_M, boL_M = slg.biorthogonal_parallel(
    [[oR_M_ps, ooR_M_bin],[oL_M_ps, ooL_M_bin]]
    )

#%% [markdown]
# ### Build types from biorthogonal matrices

#%%

boRoR_T, boLoL_T = slg.build_types_parallel(
    [
        [terms, contexts, boR_M, oR_M_ps],
        [contexts, terms, boL_M, oL_M_ps]
        ],
    min_len = params.typesMinLen,
    saveQ = True,
    filename = params.typesFileName,

)

#%%
