#######################################
# PARAMETERS FOR THE ENTIRE PROCEDURE #
#######################################

#####################
# 00 - GENERAL #
#####################

inputCorpus = "bncnorm"

inputSentences = "BNC_56-100"

# Procedure steps to perform
# (uncomment the desired steps to be performed)
stepList = [
    # "01-01_build_vocabulary",
    # "01-02_segment_sentences",
    # "01-03_extract_orthogonals",
    "02-01_build_paradigms",
    "02-01-1_complete_lattice_graph",
]

#####################
# 01 - SEGMENTATION #
#####################

# 01.01 - BUILD VOCABULARY #
############################

# Whether to resume the construction of the vocabulary from a previous
# construction
resumeQ = False

# Wether to construct the vocabulary from a fragment of the corpus only. In
# number of characters. None for entire corpus
corpusPartLength = None

# Length of the intended vocabulary (on top of initial length of atomic
# characters)
vocLen = 30000

# Whether to compute the vocabulary in parallel, increasing efficiency, but
# producing (very) slightly different results than sequential computing (since
# it is forced to first segment the corpus in as many parts as cores).
parallelVoc = True

# # Numbers of cores for computing in parallel
nCores = 24

# Save intermediate results at given iterations ([] for not saving)
interResults = [] # [(i * 10) - 1 for i in range(1, 21)]
saveFinalQ = True

# Print results of vocabulary building during execution, for control?
vocBuildPrintQ = True

# Vocabulary to be saved and then loaded to use for segmentation (to be placed in '/res/[analysis_id]/vocabularies/')

vocFileName = f"{inputCorpus}_{vocLen}{f'_p{nCores}' if parallelVoc else ''}{f'_{corpusPartLength}' if corpusPartLength != None else ''}"

# # 01.02 - SEGMENTATION #
# ########################

segFileName = f"{inputSentences}_{vocFileName}"

# Type of segmentation: sequences (seg_type = "sq") or trees (seg_type = "tr")
segType = "sq"

# How many sentences to segment ? 'None' for All
sampleSize = 10000

# Random sample of sentences?
randomSegQ = False

# Save segmentations?
saveSegQ = True

# Save orthogonals?
saveOrthoQ = True

# Minimal frequency of orthogonals to be saved (either an integer or a list of integers to produce different files)
freqMinOrtho = [10,4,0]


###############
# 02 - TYPING #
###############

# 02-01_compute_closure #
#########################

# Minimal frequency of orthogonals to be loaded
freqMinOrthoLoad = 0

orthoFileName = "bnc_ng2_10"
#f"{segType}_{segFileName}_{freqMinOrthoLoad}"

# Minimal frequency of orthogonals to be filtered from loaded file
freqMinOrthoFilter = 100

#TODO: add that if freqMinOrthoFilter<freqMinOrthoLoad don't apply filter and otherwise, change name of output file of closure

# Provide hand-picked terms and contexts?
hpTerms =  [] #["i", "you", "he", "she", "it", "we", "they"]
hpContexts = []

# Trim terms and contexts to most common terms? (None for no Trim. Not relevant if hand-picked)
trimTerms = 1000
trimContexts = 1000

# Symmetric terms and contexts? 'i' for Intersection and 'u' (default) for Union
symmetricQ = "u"

# Type of PMI: pmi, ppmi, spmi, sppmi, ssppmi, nopmi
typePMI = "ppmi"
# Alpha smoothing factor for PMI, in case of spmi or sppmi
alpha = 1

# Cut function for discretizing matrices: "cut_thres", "cut_mean"
cutFunc = "cut_mean"
# Cut threshold (in case of cut_thres) or factor (in case of cut_mean)
thres = 1 #1e-5

# Minimal number of units of rows to remove from binary matrix (Default: 2)
reduceThres = 2

# Minimal length of types
typesMinLen = 2

typesFileName = f"{orthoFileName}_{typePMI}{alpha}_{cutFunc[4:]}-{thres}"

# 02-01-01_complete_lattice_graph #
###################################

# Plot only types having subtypes relations?
subtypeFilter = True

# Plot only biggest type for subtypes having the same orthogonal type?
subtypeReduction = True