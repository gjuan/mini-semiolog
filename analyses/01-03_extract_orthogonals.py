#%% [markdown]
# # Extracting Orthogonals
#%% [markdown]
# ## Initialize
#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

#%%
# Load segmentations

segs = slg.load_segs(f"{params.segType}_{params.segFileName}")

orthogonals = slg.extract_orthogonals(
    segs,
    saveQ = params.saveOrthoQ,
    filename = f"{params.segType}_{params.segFileName}",
    freq_min = params.freqMinOrtho,
    )

# %%
