#%% [markdown]
# # Computing probabilistic paradigms

#%% [markdown]
# ## Initialize
#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

from sklearn.preprocessing import normalize
from collections import Counter
import time
import random
import pandas as pd

#%%
# Load sentences and load orthogonals

sents = slg.load_test_sents(
    params.inputSentences
)

orthogonals = slg.load_orthogonality_data(
    filename = params.orthoFileName,
    freq_min = params.freqMinOrthoFilter
    )

# %%

terms, contexts = slg.build_terms_contexts(
    orthogonals
)

#%% [markdown]
# ## Matrices

# ### Term-Context Matrix

#%%
oR_M = slg.build_term_context_matrix(
    terms,
    contexts,
    orthogonals,
    normalizeQ=True)

oL_M = oR_M.tocsc(copy=True).T


#%%

print("PMI R...")
oR_M_pmi = slg.build_pmi_matrix(
    oR_M,
    type = params.typePMI,
    alpha = params.alpha,
    normalizeQ = True,
    )

print("PMI L...")
oL_M_pmi = slg.build_pmi_matrix(
    oL_M,
    alpha = params.alpha,
    type = params.typePMI,
    normalizeQ = True,
    )

#%%

oR_M_norm = normalize(oR_M_pmi)
oL_M_norm = normalize(oL_M_pmi)
# %%

oR_rho = oR_M_norm.dot(oR_M_norm.T)
oL_rho = oL_M_norm.dot(oL_M_norm.T)


#%%
terms_dict = {terms[n]:sum(oR_M_pmi[n].data) for n in range(oR_M_pmi.shape[0])}

contexts_dict = {contexts[n]:sum(oL_M_pmi[n].data) for n in range(oL_M_pmi.shape[0])}

orthogonals_dict = gfs.normalize_dict(orthogonals)
# %%

def construct_seg_class_raw(
    sent_seg:str,
    len_class=None,
    score_thres=0
    ):
    
    if sent_seg not in contexts:
        class_raw_L = []
    else:
        sent_seg_vector_L = oL_rho[contexts.index(sent_seg)]

        class_raw_L = sorted(
            [(contexts[t],s) for t,s in zip(
            sent_seg_vector_L.nonzero()[1],
            sent_seg_vector_L.data)],
            key=lambda x:x[1],reverse=True)
    
    if sent_seg not in terms:
        class_raw_R = []
    else:

        sent_seg_vector_R = oR_rho[terms.index(sent_seg)]
        class_raw_R = sorted(
            [(terms[c],s) for c,s in zip(
            sent_seg_vector_R.nonzero()[1],
            sent_seg_vector_R.data)],
            key=lambda x:x[1],reverse=True)
    return ([t for t in class_raw_L if t[1]>score_thres][:len_class],[t for t in class_raw_L if t[1]>score_thres][:len_class])

def filter_seg_classes(
    trigram,
    LR_paradigms,
    orthogonals,
    prob_thres = 1,
    score_thres = 0,
    len_class = None,
    output_scoreQ = False
    ):
    L_parad = dict([t for t in LR_paradigms[0] if (trigram[0],t[0]) in orthogonals or trigram[0]=="#"])
    R_parad = dict([t for t in LR_paradigms[1] if (t[0],trigram[-1]) in orthogonals or trigram[-1]=="#"])

    LR_term_intersection = set(L_parad.keys()).intersection(R_parad.keys())
    LR_term_intersection_scores = sorted(
        list(
        gfs.normalize_dict(
        dict(
            [
            (term,
            (L_parad[term]+R_parad[term])/2)
            for term in LR_term_intersection]
                )).items()
    ),key=lambda x:-x[1])

    LR_term_intersection_neighborhood = []
    if len(LR_term_intersection_scores)>0:
        aggregate_prob = 0
        n_top = 0
        while aggregate_prob < prob_thres and n_top<len(LR_term_intersection_scores):
            LR_term_intersection_neighborhood.append(LR_term_intersection_scores[n_top])
            aggregate_prob += LR_term_intersection_scores[n_top][1]
            n_top += 1

    # if trigram[1] not in [k for k,v in LR_term_intersection_neighborhood]:
    

    ranked_intersection_class = sorted(LR_term_intersection_neighborhood,
        key=lambda x:x[-1],reverse=True)
    if output_scoreQ:
        ranked_intersection_class_cut = [(k,v) for k,v in ranked_intersection_class if v>score_thres][:len_class]
        if len(ranked_intersection_class_cut)==0:
            ranked_intersection_class_cut.append((trigram[1],1))
    else:
        ranked_intersection_class_cut = [k for k,v in ranked_intersection_class if v>score_thres][:len_class]
        if len(ranked_intersection_class_cut)==0:
            ranked_intersection_class_cut.append(trigram[1])
    return ranked_intersection_class_cut

#%%
# Individual paradigmatic extraction

test_sent = "i have made my plans and i must stick to them"
test_sent = random.choice(sents)
print(test_sent+"\n")

sent_split = test_sent.split()

sent_classes_raw = [construct_seg_class_raw(
    seg,
    None,
    0.25
    ) for seg in sent_split]

# gfs.clear_df(pd.DataFrame([[(k,round(v,2)) for k,v in l] for l,r in sent_classes_raw]).T)


# #%%

output_scoreQ = False
sent_tg = gfs.subsequences(["#"]+sent_split+["#"],3)

sent_classes_filter = []
for trigram,LR_paradigms in zip(sent_tg,sent_classes_raw):
    ranked_intersection_class = filter_seg_classes(
        trigram,
        LR_paradigms,
        orthogonals_dict,
        prob_thres = 1,
        score_thres = 0.05,
        len_class = 10,
        output_scoreQ=output_scoreQ
        )
    sent_classes_filter.append(ranked_intersection_class)


if output_scoreQ:
    for _ in range(3):
        print(" ".join([k for k,v in [random.choice(parad) for parad in sent_classes_filter]]))

    sent_parads = gfs.clear_df(pd.DataFrame([[(k,round(v,2)) for k,v in lr] for lr in sent_classes_filter]).T)

else:
    for _ in range(3):
        print(" ".join([k for k in [random.choice(parad) for parad in sent_classes_filter]]))
    sent_parads = gfs.clear_df(pd.DataFrame(sent_classes_filter).T)
sent_parads





#%%
# Build paradigms of sentences random batch

def compute_ortho_types(n_iter_test_sent):
    n_iter = n_iter_test_sent[0]
    if str(n_iter)[-1:]=="0":
        print(n_iter)

    test_sent = n_iter_test_sent[1]
    sent_split = test_sent.split()

    sent_classes_raw = [construct_seg_class_raw(
        seg,
        None,
        0.25
        ) for seg in sent_split]


    sent_tg = gfs.subsequences(["#"]+sent_split+["#"],3)

    sent_classes_filter = []
    for trigram,LR_paradigms in zip(sent_tg,sent_classes_raw):
        ranked_intersection_class = filter_seg_classes(
            trigram,
            LR_paradigms,
            orthogonals_dict,
            prob_thres = 1,
            score_thres = .25,
            len_class = 10,
            output_scoreQ=output_scoreQ
            )
        sent_classes_filter.append(ranked_intersection_class)



    ortho_types_sent = [(tuple(sorted(type_l)),tuple(sorted(type_r))) for type_l,type_r in gfs.subsequences(sent_classes_filter,2) if min(len(type_l),len(type_r))>=2]

    return ortho_types_sent

start = time.perf_counter()
ortho_types = gfs.multiprocessing(compute_ortho_types,enumerate(random.sample(sents,10000)))
print(f"ortho_types computed in {time.perf_counter()-start}")

ortho_types_flat = []
for s in ortho_types:
    for o in s:
        ortho_types_flat.append(o)
ortho_types = ortho_types_flat
#%%
ortho_types_ranked = Counter(ortho_types)

ortho_types_ranked.most_common(10)

#%%

ranked_types_L = [t for t,v in Counter([l for (l,r) in ortho_types]).most_common() if v>=2]
ranked_types_R = [t for t,v in Counter([r for (l,r) in ortho_types]).most_common() if v>=2]

#%%

type_L = ranked_types_L[0]
print(type_L)

type_L_oR = set.union(*[set(r) for l,r in ortho_types if l == type_L])
type_L_oR

type_L_ooR = set.union(*[set(l) for l,r in ortho_types if set(r).issubset(type_L_oR) and l in ranked_types_L[:30]])
gfs.clear_df(pd.DataFrame([list(type_L_ooR),list(type_L_oR)]).T)

#%%

ortho_types_top = [(l,r) for l,r in ortho_types if l in ranked_types_L and r in ranked_types_R]
len(ortho_types_top)
#%%
ortho_n = 500
boRoR_T = ortho_types_top[:ortho_n]

boRoR_graph = slg.build_ortho_graph(
    boRoR_T,
    side = "R",
    ortho = True,
    subtype_filter = True,
    filter_sT = False,
    )

lattice_graph = slg.lattice_graph(
    # boLoL_graph,
    boRoR_graph,
    # complete_graph,
    fname = paths.lattice_graphs+"prob_parad_test",
    dependenciesQ = False,
)
lattice_graph.view()
# %%
