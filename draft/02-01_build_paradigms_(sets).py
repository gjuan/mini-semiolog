#%% [markdown]
# # Computing probabilistic paradigms

#%% [markdown]
# ## Initialize
#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

import itertools
from collections import Counter
from scipy.stats import entropy
import time
import random
import numpy as np
import functools

#%%
# Load segmentations
sents = slg.load_test_sents(
    params.inputSentences
)


orthogonals = slg.load_orthogonality_data(
    filename=params.orthoFileName,
    freq_min=freqMinOrthoFilter
    )

# %%

terms, contexts = slg.build_terms_contexts(
    orthogonals
)

#%% [markdown]
# ## Matrices

# ### Term-Context Matrix

#%%
oR_M = slg.build_term_context_matrix(
    terms,
    contexts,
    orthogonals,
    normalizeQ=True)

oL_M = oR_M.T

#%%

oR_M_bin, ooR_M_bin = [
    slg.build_bin_matrix(
        matrix,
        cut_func = "cut_thres",
        thres=0,
        )
    for matrix in [oR_M, oR_M.T]
]

oL_M_bin, ooL_M_bin = [
    slg.build_bin_matrix(
        matrix,
        cut_func = "cut_thres",
        thres=0,
        )
    for matrix in [oL_M, oL_M.T]
    ]

#%%

oR_M_ps, oL_M_ps = slg.powerset_M_parallel(
    [oR_M_bin, oL_M_bin],
    reduce_thres = params.reduceThres,
    bound = 20,
)


#%%
ps_terms = [set(oL_M_ps[i].indices) for i in range(oL_M_ps.shape[0])]

terms_dict = {n:sum(oR_M[n].data) for n in range(oR_M.shape[0])}

contexts_dict = {n:sum(oL_M[n].data) for n in range(oL_M.shape[0])}

orthogonals_dict = gfs.normalize_dict(orthogonals)


#%%


####################################


t_min = 2
t_max = 10
c_min = 2
c_max = 10

def random_ortho_parads(ps_terms, t_dict,c_dict,tc_dict,iter=None):
    if iter != None and str(iter)[-3:]=="000":
        print(f"{iter}\n")
    c_universe = set()
    ps_terms = [s for s in ps_terms if t_min<=len(s)<=t_max]
    while len(c_universe) < c_min:
        t_parad_n = random.randint(t_min,t_max)
        # t_parad_raw = list(zip(*random.sample(t_dict.items(),t_parad_n)))
        t_parad_random = random.choice(ps_terms)
        t_parad_raw = [t_parad_random,[t_dict[t] for t in t_parad_random]]
        c_universe = set.intersection(*[set(oR_M[t_i].indices) for t_i in t_parad_raw[0]])

    t_parad = tuple(sorted([terms[n] for n in t_parad_raw[0]]))

    t_parad_prob = sum(t_parad_raw[1])
        

    c_parad_n = random.randint(c_min,min(c_max,len(c_universe)))


    c_parad_i = random.sample(c_universe,c_parad_n)
    c_parad = tuple(sorted([contexts[c_i] for c_i in c_parad_i]))

    c_parad_raw = [c_dict[c_i] for c_i in c_parad_i]

    c_parad_prob = sum(c_parad_raw)


    tc_parad = [(t,c) for t in t_parad for c in c_parad]
    tc_parad_prob = sum([tc_dict.get(tc,0) for tc in tc_parad])
    
    # entropy_t = entropy(t_parad_raw[1])
    # entropy_c = entropy(c_parad_raw)
    # entropy_tc = entropy_t*entropy_c

    c_t_cond_prob = (tc_parad_prob/t_parad_prob)#*entropy_tc
    t_c_cond_prob = (tc_parad_prob/c_parad_prob)#*entropy_tc

    return ((t_parad,c_parad),(c_t_cond_prob,t_c_cond_prob,c_t_cond_prob*t_c_cond_prob))

start = time.perf_counter()

n_sample = 30000
random_ortho = gfs.multithreading(functools.partial(random_ortho_parads,ps_terms,terms_dict,contexts_dict,orthogonals_dict),range(n_sample))

# random_ortho = []
# for iter in range(n_sample):
#     indiv_ortho = random_ortho_parads(ps_terms, terms_dict,contexts_dict,orthogonals_dict,iter)
#     random_ortho.append(indiv_ortho)

print(f"End of calculation in {time.perf_counter()-start}")



#%%

ortho_rank = sorted(random_ortho,key=lambda x:x[1][2],reverse=True)
ortho_rank[:30]

#%%
l_types = Counter()
for (l,r),v in ortho_rank:
    l_types[l] += v[1]

r_types = Counter()
for (l,r),v in ortho_rank:
    r_types[r] += v[1]

bi_ortho_class_l = []
for item,v in l_types.most_common():
    ortho_class = [set(r) for (l,r),v in ortho_rank if l == item]
    ortho_class = [s for s in ortho_class if s!=set()]
    ortho_class_union = tuple(sorted(list(set.union(*ortho_class))))
    bi_ortho_class_l.append((item, ortho_class_union))

bi_ortho_class_r = []
for item,v in r_types.most_common():
    ortho_class = [set(l) for (l,r),v in ortho_rank if r == item]
    ortho_class = [s for s in ortho_class if s!=set()]
    ortho_class_union = tuple(sorted(list(set.union(*ortho_class))))
    bi_ortho_class_r.append((ortho_class_union,item))
print("Done!")

#%%
ortho_n = 100
boRoR_T = bi_ortho_class_l[:ortho_n]+bi_ortho_class_r[:ortho_n] #+ [t for t,p in ortho_rank[:500]]
# boRoR_T = [t for t,p in ortho_rank[:ortho_n]]

boRoR_graph = slg.build_ortho_graph(
    boRoR_T,
    side = "R",
    ortho = True,
    subtype_filter = True,
    filter_sT = True,
    )

lattice_graph = slg.lattice_graph(
    # boLoL_graph,
    boRoR_graph,
    # complete_graph,
    fname = paths.lattice_graphs+"prob_parad_test",
    dependenciesQ = True,
)
lattice_graph.view()
# %%
