#%% [markdown]
# # Computing probabilistic paradigms

#%% [markdown]
# ## Initialize
#%%
if __name__ == "__main__":
    import os
    os.chdir("../")
from settings.init import *

import itertools
from collections import Counter
from scipy.stats import entropy
import time

#%%
# Load segmentations
# segs = slg.load_test_sents(
#     params.inputSentences
# )

segs = slg.load_segs(
    "sq_BNC_56-100_bncnorm_30000_p24"
)

orthogonals_all = slg.extract_orthogonals(
    segs,
    saveQ = False, # params.saveOrthoQ,
    filename = f"{params.segType}_{params.segFileName}",
    freq_min = params.freqMinOrtho,
    )

# orthogonals_all = slg.load_orthogonality_data("tr_BNC_56-100_bncnorm_30000_p24_10")

# %%
orthogonals = {k:v for k,v in orthogonals_all.items() if v>=150}

#%%
orthogonals_norm = gfs.normalize_dict(orthogonals)

#%%
c_parad = {"ed","ing","s"}

ct_parad = [tc for tc in orthogonals_norm.items() if tc[0][1] in c_parad]
ct_parad
# %%
