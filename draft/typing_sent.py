
#%%

# ortho_rank_R = [k for k,v in sorted(random_ortho,key=lambda x:x[1][0],reverse=True)]
# ortho_rank_L = [k for k,v in sorted(random_ortho,key=lambda x:x[1][1],reverse=True)]
ortho_rank_LR = [k for k,v in sorted(random_ortho,key=lambda x:x[1][2],reverse=True)]
# %%
ortho_rank_LR[-5:]
# %%
test_sent = "i have made my plans and i must stick to them"
test_sent = random.choice(segs)
print(test_sent)

sent_split = test_sent.split()
sent_bg = gfs.subsequences(sent_split,2)



# sent_R = []
# for bg in sent_bg:
#     sent_R.append([(l,r) for l,r in ortho_rank_LR if bg[0] in l and bg[1] in r])

sent_R = []
for bg in sent_bg:
    sent_R.append([(l,r) for l,r in ortho_rank_LR if bg[0] in l and bg[1] in r])


sent_R_union = []
for seg in sent_R:
    union_l = set.union(*[set(l) for l,r in seg]) if len(seg)>0 else []
    union_r = set.union(*[set(r) for l,r in seg]) if len(seg)>0 else []
    sent_R_union.append((union_l,union_r))



# for bg_n in range(len(sent_bg)-1):
#     tL = [set(r) for l,r in sent_R[bg_n]]
#     tR = [set(l) for l,r in sent_R[bg_n+1]]
#     tLR_raw = [tuple(sorted(list(set.intersection(tl,tr)))) for tl in tL for tr in tR]
#     tLR = sorted(list(set(tLR_raw)),key=lambda x:-len(x))
#     tLR_count = Counter(tLR_raw)
#     print(sent_bg[bg_n][1])
#     # print(f"{tLR[:1]}")
#     print(f"{tLR_count.most_common()[:2]}\n")


for bg_n in range(len(sent_bg)-1):

    tLR_raw = set(sent_R_union[bg_n][1]).intersection(set(sent_R_union[bg_n+1][0]))

    tLR = tuple(sorted(list(tLR_raw)))
    print(tLR)


# %%
from collections import defaultdict

seg_types = defaultdict(list)
for seg in sent_split:
    tL = [(l,r) for l,r in ortho_rank_LR if seg in r]
    tR = [(l,r) for l,r in ortho_rank_LR if seg in l]
    seg_types[seg].append(tL)
# %%
from sklearn.preprocessing import normalize

oR_M_norm = normalize(oR_M)

#%%
test_sent = "i have made my plans and i must stick to them"
test_sent = random.choice(segs)
print(test_sent)

sent_split = test_sent.split()
sent_bg = gfs.subsequences(sent_split,2)

all_pre_parad = []
for bg in sent_bg:
    segment = bg[0]

    segment_ortho = bg[1]
    if segment in terms and segment_ortho in contexts:
        seg_vector = oR_M_norm[terms.index(segment)]

        pre_parad = sorted(list(zip(terms,oR_M_norm.dot(seg_vector.T).data)),key=lambda x:x[-1],reverse=True)

        context_i = contexts.index(segment_ortho)
        final_pre_parad = []
        n_iter = 0
        while len(final_pre_parad)<10 and n_iter<len(pre_parad)-1:
            n_iter += 1
            seg_parad = pre_parad[n_iter][0]

            seg_i = set(oR_M[terms.index(seg_parad)].indices)
            if context_i in seg_i:
                final_pre_parad.append(seg_parad)
    else:
        final_pre_parad = []
        print("term o element not in list")
    all_pre_parad.append((segment,final_pre_parad))
all_pre_parad
# %%
