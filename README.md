# Mini-SemioLog: Framework for semiological analysis

## Licence
Attribution-NonCommercial 4.0

International (CC BY-NC 4.0)

## Version
 README version: `0.1.1`

Python version: `3.7.4 64-bit`

# General execution of the procedure

All the steps of the general procedure of corpus analysis for a particular
corpus and sets of parameters can be performed by executing the script
`procedure.py` at the root. The id of the desired analysis should be then entered as value for the variable `analysis_id` in that script (eg: `'bnc_s'`).

Two things are required to run a particular analysis:
1. A folder in `/res/` with the same name of the analaysis id, containing:
     1. A folder `_corpus_`, containing a (pre-procesed) corpus (in `txt` format).
     2. A folder `_sentences_`,  containing a list of sentences (typically, from the corpus in `1.1`) to be segmented.
     3. NB: If the particular analysis does not intend to start from the first step of the procedure, but from a more advanced step (eg: `01_03_extract_orthogonals`), then the id folder in `/res/` should contain the necessary input data in the corresponding folder (eg: `/bnc_s/segmentations/sq_BNC_56-100_bnc_s_30_p4.txt`)
2. A folder in `/analyses/` with the same name of the analysis id, containing a file `params.py`, with all the corresponding parameters necessary to the procedure. In particular, this file should contain:
   1. The name of the corpus file (without extension), under the variable `inputCorpus`
   2. The name of the sentences file (without extension), under the variable `inputSentences`
   3. The list of the steps of the procedure to be performed, under the variable `stepList`  (correspondig to the files in `/analyses/`)
   4. [NB:  The place of the file `params.py` for each particular analysis is susceptible to change in upcoming versions]


## `/procedure.py`
### Description
After initialiazing settings and configurations, the script runs the analysis, step by step.

### Possible modifications
The user may modify:
1. The id of the particular analysis to be performed
2. Manually entered parameters, overrunning initialized parameters.

### Cotent of the script
The script contains four parts:
1. Assignation the id of the particular analysis to the variable `analysis_id`
   1. `analysis_id`: The most important parameter. Determines the environment
      for the analysis of a specific dataset. The value of this parameter is a
      string corresponding to the name of a folder in `/analyses/`, where
      specific parameters for the intended environment are defined in a file
      named `params.py` (Eg: `/analyses/bnc/params.py`). Another folder of the
      same name should exist in `/res/`, containing the relevant input folders/files (see "General execution of the procedure" above)
2. Initialization of settings and parameters
   * It loads all functions of `init.py` in `/settings/`
3. Evaluation of parameters from the command line (if any), in case the file is run through the terminal.
    * These parameters overrun the ones loaded in the initialization
4. Manually entered parameters, overrun
    * Eg: `params.alpha=.75`
5. Execution of the list of steps defined in `params.stepList`

## `/settings/init.py`
### Description
Loads the global settings as well as parameters of the specific analysis.

### Possible modifications
Not to be modified by the user.

### Content of the script
1. Loads the configurations in `/settings/config.py`
2. Loads the paths in `/settings/paths.py`
3. Loads the general functions in `/lib/functions.py`.
    * These functions are to be called under the module `gfs`.
    * Eg: `gfs.flatten(my_list)`
4. Loads the source code (library of functions) "semiolog" in `/src/semiolog.py`.
    * These functions are to be called under the module `slg`.
    * Eg: `slg.build_vocab()`
5. Loads the parameters of the specific analysis defined in the configuration `config.analysis_id`
    * These parameters may be overrun by parameters manually entered when executing `/procedure.py` through the command line or manually entered in that file.


## `/settings/config.py`

### Description
Global options for the entire environment

### Possible modifications
The user may modify:
1. `known_clusters`: Add (sub)strings identifying the name of clusters, to
   recognize if the execution is taking place on a cluster or on a personal
   computer
3. Any other global configuration the user might consider necessary declaring
   * Eg: `pd.options.display.float_format`: pandas display of floats

### Content of the script
1. Loads dependencies.
2. Recovers python version, id of host and tries to determine if current execution happens in a cluster.
3. Determines analysis id.
4. Prints all info in 2 and 3.

## `/settings/paths.py`

### Descritpion
Defines file locations. All paths are defied relative to the root of the project's folder (named `mini-semiolog` by default)

### Possible modifications
The user may modify or add locations only if needed.

### Content of the script
1. Loads dependencis and config.
2. Defines the paths for libraires, corpus and sentences, intermediate results and final results. More precisely, it defines the following variables, which can be globally called by prepending `paths.` (Eg: `paths.vocabularies`):
    * `res`
    * `analysis_dir`
    * `lib`
    * `corpus`
    * `sentences`
    * `vocabularies`
    <!-- * `segmentations` -->
    <!-- * `orthogonals` -->
    <!-- * `matrices` -->
    <!-- * `segment_graphs` -->
    <!-- * `lattice_graphs` -->
    <!-- * `types` -->
    <!-- * `typed_sent` -->

## `/lib/functions.py`

### Descritpion
General functions often used for the manipulation, visualization or storage of
data, but not representing operations specific to the semiological procedure.
* These functions are to be called under the module `gfs`.
* Eg: `gfs.flatten(my_list)`

### Possible modifications
The user may add functions if needed.

### Content of the script
1. Loads dependencies
2. Define functions

## `/src/semiolog.py`

### Descritpion
Library of functions representing operations specific to the semiological procedure.
* These functions are to be called under the module `slg`.
* Eg: `slg.build_vocab()`

### Possible modifications
The user may add functions if needed.

### Content of the script
1. Load dependencies
2. Load functions and settings
3. Define functions, ordered by the different parts and steps of the procedure:
   * 00 - GENERAL
   * 01 - SEGMENTATION FUNCTIONS
     * 01.01 Building Vocabulary
     * 01.02 Segment Sentences
     * 01.03 Extract Orthogonals
   * 02 - TYPING FUNCTIONS
   * [To be completed]

## `/analyses/[analysis_id]/params.py`

### Descritpion
Parameters for the procedure performed in the environment of the analysis `analysis_id`

NB: The location of `params.py` for each particular analysis is susceptible to
be changed in upcoming versions

### Possible modifications
The user may modify all the parameters.

### Content of the script
1. Define the following parameters:
   *  00 - GENERAL
      *  `inputCorpus`: Name of the input corpus. Name of the txt file, without extension (eg: `bnc_s`). The corresponding txt file should be placed in the path defined in `paths.corpus` (`/settings/paths.py`)
      *   `inputSentences`: Sentences to be segmented. Name of the txt file, without extension (eg: `BNC_56-100`). The corresponding txt file should be placed in the path defined in `paths.sentences` (`/settings/paths.py`)
      *   `stepList`: List of procedure steps to be performed. Each step correspond to the name (without extension) of a file in `/analyses/` (eg: `01-02_segment_sentences`)
   *  01 - SEGMENTATION
      *  01.01 - BUILD VOCABULARY
         *  `resumeQ`: Whether to resume the construction of the vocabulary from a previous construction (default: `False`)
         *  `resumeQ`: Wether to construct the vocabulary from a fragment of the corpus only. In number of characters. None for entire corpus (default: `None`)
         *  `voc_len`: Length of the intended vocabulary (on top of initial length of atomic characters). (default: `10000`)
         *  `parallel_voc`: Whether to compute the vocabulary in parallel, increasing efficiency, but producing (very) slightly different results than sequential computing (since it is forced to first segment the corpus in as many parts as cores). (default: `True`)
         <!-- *  `n_cores`: number of cores for computing in parallel -->
         *  `inter_results`: Save intermediate results at given iterations ([] for not saving). (default: `[(i * 10) - 1 for i in range(1, 21)]`)
         *  `save_finalQ`: Save final vocabulary? (default: `True`)
         *  `voc_build_printQ`: Print results of vocabulary building during execution, for control? (default: `True`)
      *  01.02 - SEGMENTATION
         *  `inputVoc`: Vocabulary to be used for segmentation (name of a `csv` file in `/res/[analysis_id]/vocabularies/`, without extension)

[To be continued]
         

# Steps of the procedure

All the files corresponding to the steps of the procedure run `/settings/init.py`. In case they are executed as individual files (instead of being called through `/procedure.py`), they change the current working directory to the root of the project first.

All the files corresponding to procedure steps are to be placed in `/analyses/`, and are supposed, in principle, to be valid for every analysis. The filenames are supposed to be prefixed by the number of the part of the procedure, followed by the number of the step within that part (eg: `01-01_build_vocabulary`).

## `01-01_build_vocabulary`

### Descritpion
Build vocabulary out of a corpus

### Possible modifications
The user may modify the parameters of the functions.

### Content of the script
1. Load corpus calling `slg.load_corpus()`
2. Build vocabulary calling `slg.build_vocabulary([chain])` (where `[chain]` is the name of the variable where the loaded corpus was stored)

## `01-02_segment_sentences`

### Descritpion
Segment sentential units into sequences or trees

### Possible modifications
The user may modify the parameters of the functions.

### Content of the script
1. Load vocabulary calling `slg.load_vocabulary()`
2. Load sentential units to be segmented, calling `load_test_sents()`
3. Build vocabulary calling `slg.build_vocabulary([chain])` (where `[chain]` is the name of the variable where the loaded corpus was stored)

[To be continued]

# semiolog commands

## `load_corpus`

`load_corpus(file = params.inputCorpus, length = params.corpus_part_length)`

### Parameters
* `file` : `str`
  * Name of the corpus file (without extension, `txt` is assumed)
  * default : `params.inputCorpus`
* `length` : `int`
  * Number of characters to load from file (`None` for loading the entire file)
  * default: `corpus_part_length`

### Returns
* `chain`: `str`
  * The entire corpus as a continuous string

### Prints
* Confirmation of success with elapsed time
  
## `build_vocabulary`

`build_vocabulary(chain, printQ=params.voc_build_printQ, parallelQ=params.parallel_voc, n_cores=params.n_cores, voc_len=params.voc_len, inter_results=params.inter_results, resumeQ = params.resumeQ, save_finalQ = params.save_finalQ)`

### Parameters
* `chain` : `str`
  * 
* `printQ` : `bool`
  * 
  * default: `params.voc_build_printQ`

### Returns
* `voc`: `dict`
  * 

### Prints
* 
* Confirmation of success with elapsed time
  
## `load_vocabulary`

`load_vocabulary(file = f"{paths.vocabularies}{params.inputVoc}.csv")`

### Parameters
* `file` : `str`
  * default: `params.inputVoc`

### Returns
* `voc_A`: `dict`

### Prints
* Information of the file being loaded
* Confirmation of success

## `load_test_sents`

`load_test_sents(file = params.inputSentences)`

### Parameters
* `file` : `str`
  * default: `params.inputSentences`

### Returns
* `sents`: `list` of `str`

### Prints
* Information of the file being loaded
* Confirmation of success

[To be cotinued]