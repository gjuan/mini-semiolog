# Main file for executing an analysis procedure

# Id of analysis
# (enter the name of the desired analysis)
analysis_id = "bnc"

# Initialize
from settings.init import *

# Parameters from command line
import sys
if sys.argv[0] == 'procedure.py':
    cl_params = sys.argv[1:]
    for param in cl_params:
        exec(param)

# Parameters overrunning initialization parameters
# params.alpha = .75

# Perform procedure steps
if __name__ == "__main__":
    slg.execute_procedure(params.stepList)