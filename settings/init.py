# INITIAL MODULES AND CONFIGURATIONS FOR EVERY FILE

import settings.config as config
import settings.paths as paths
import lib.functions as gfs
import src.semiolog as slg

exec(f"import analyses.{config.analysis_id}.params as params")