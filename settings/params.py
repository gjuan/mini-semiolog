#######################################
# PARAMETERS FOR THE ENTIRE PROCEDURE #
#######################################

#####################
# 01 - SEGMENTATION #
#####################

# 01.01 - BUILD VOCABULARY #
############################

# Input Corpus. Possible options: bncReNorm (default), bncTest, ArAdd
inputCorpus = "bncReNorm"
resumeQ = False
corpus_part_length = 1000  # in number of characters. None for entire corpus
voc_len = 100
# Whether to compute the vocabulary in parallel, increasing efficiency, but producing (very) slightly different results than sequential computing (since it is forced to first segment the corpus in as many parts as cores).
parallel_voc = True
n_cores = 4
# Save intermediate results at given iterations ([] for not saving)
inter_results = [(i * 10) - 1 for i in range(1, 21)]
save_finalQ = True
# Print results of vocabulary building during execution, for control?
voc_build_printQ = True

# 01.02 - SEGMENTATION #
########################

# inputVoc = "voc_A_bncReNorm_p_17000_inter_100M"
inputVoc = "voc_A_bncReNorm_p_30K_inter"

# Possible sents:
# BNC_03-29_1378798
# BNC_30-55_1003426
# BNC_56-100_1296465
# BNC_151-250_908738
# BNC_251-All_239073
inputSentences = "BNC_56-100_1296465"

# How many sentences to segment in bulk_segment? 'None' for All
bulkSize = 100

segment_printQ = True

# Default list of already segmented chains
segChains =  "chains_BNC_56-100_1296465_voc30K"

###############
# 02 - TYPING #
###############


# Possible Inputs: COCA, BNC_Chars, BNC_Chars_SG_1G, BNC_Chars_SG_2G, BNC_SG_2G_4, BNC_SG_2G_4_corr
inputData = "BNC_Chars_SG_1G"

# Symmetric terms and contexts? 'i' for Intersection and 'u' (default) for Union
symmetricQ = "u"

# Provide hand-picked terms and contexts?
hp_terms = []  # ["i", "you", "he", "she", "it", "we", "they"]
hp_contexts = []

# Trim terms and contexts to most common terms? (None for no Trim. Not relevant if hand-picked)
Trim_terms_Length = 10000
Trim_contexts_Length = 10000


# Which normalizing measure to use: possible measures: "freq", "prob", "self_inf", "pmi", "binary" (transposable) "norm_freq", "log_freq", "cond_prob" (non transposable)
# new: "m_log_freq", "m_binary", "m_frequency"
measure = "m_frequency"

# Delete empty lines and columns from Matrices (and corresponding alphabets)?
# WARNING: Keep it in False until it is dealt with
# skimQ = False


# Cut function for discretizing matrices: "cut_thres", "cut_mean_positive"
cut_func = "cut_mean_positive"
# Cut threshold in case of cut_thres
thres = 0

# Weight for mean at binarizaiton of orthogonal matrices. The higher, the stricter. 1 is neutral. Default = 1
# In the case fo global binarazation where a general threshold is needed, weight accounts for the general threshold. The Default value pour PMI should be 0.
weight = 1
weight_o = 1
weight_oo = 1
thres = 0
const_thres = False
# %%
