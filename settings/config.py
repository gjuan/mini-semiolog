#############################################
# GLOBAL OPTIONS FOR THE ENTIRE ENVIRONMENT #
#############################################

# Load analysis id
from procedure import analysis_id

# Load dependencies
import socket
import sys
import pandas as pd

# Id of host and test for cluster (add cluster id to the known list)
python_version = sys.version.replace('\n','- ')
host_name = socket.gethostname()
known_clusters = {"eu-"}
clusterQ = any([cluster_id in host_name for cluster_id in known_clusters])

# Other custom global configurations
# Display numbers in pandas DataFrame in decimal format
pd.options.display.float_format = "{:.6f}".format

# Print settings
print("Global Settings")
print("===============")

print(f"Host name: {host_name}")
print(f"Cluster: {clusterQ}")
print(f"Python version: {python_version}")
print(f"Analysis Dataset: {analysis_id}")

print("\n")
