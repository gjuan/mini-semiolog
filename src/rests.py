# # 01-02-b Evaluate Segmentantions #
# ###################################


# def make_gold_sent(sent):
#     word_sent = sent.split()
#     gold_sent = []
#     for n in range(len(word_sent)):
#         pre = len("".join(word_sent[:n]))
#         gold_sent.append((word_sent[n], (pre, pre + len(word_sent[n]))))
#     return gold_sent

# def supervise_all(sent, decomp=True):
#     s_split = sent.split()
#     s_compose = []
#     for i in range(len(s_split)):
#         for j in range(len(s_split) - i):
#             c = "".join(s_split[i : i + j + 1])
#             s_compose.append(c)

#     s_decompose = []
#     if decomp:
#         for w in s_split:
#             for i in range(len(w)):
#                 for j in range(len(w) - i):
#                     wd = w[i : i + j + 1]
#                     s_decompose.append(wd)

#     return s_split + s_compose + s_decompose



# def segment_sent(sent_index, voc, weight_function=invLogLen, printQ=False, print_summaryQ= False, graph_dataQ = False,supervisionQ = False, graphQ = False):
#     if printQ:
#         print(f"{sent_index}")
#     if type(sent_index) == tuple:
#         sent = sent_index[1]
#     else:
#         sent = sent_index
#     chain = "".join(sent.split())
#     lSt = len(chain)
#     tree_data = chain2tree(sent, voc, weight_function)
#     if graph_dataQ:
#         gold_sent = make_gold_sent(sent)
#         gold_sent_in_tree = [i for i in gold_sent if i in tree_data.nodes()]
#         voc_in_tree = [n for n in tree_data.nodes() if n[0] not in voc]
#         if supervisionQ:                    
#             gold_places=[]
#             for list in [[c for c in range(*t)] for k,t in gold_sent_in_tree]:
#                 gold_places += list
#             rests = ''.join([chain[i] if i not in gold_places else ' ' for i in range(len(chain))]).split()
#             supervision_chars = 100 * len("".join([l for l, k in gold_sent_in_tree])) / lSt
#             supervision_words = 100 * len(gold_sent_in_tree) / len(gold_sent)
#     result = [tree_data]
#     if graph_dataQ:
#         result.extend([gold_sent_in_tree, voc_in_tree])
#         if supervisionQ:
#             result.extend([sent,lSt, supervision_chars, supervision_words, rests])
#     if print_summaryQ:
#         if not supervisionQ:
#             print("Supervision was not computed. Please activate it thorugh the option 'supervisionQ = True'")
#         else:
#             print('\nSUMMARY:')
#             print(f"Char Length: {lSt} chars")
#             print(f"Word Length: {len(gold_sent)} words")
#             print(f"Supervision Characters: {round(supervision_chars,2)} % correct")
#             print(f"Supervision Words: {round(supervision_words,2)}  % correct\n")
#     if graphQ:
#         if graph_dataQ:
#             if not supervisionQ:
#                 print("Supervision was not computed. Please activate it thorugh the option 'supervisionQ = True'. Plotting graph with non-supervised data.")
#                 seg_tree_graph = graph_tree(tree_data)
#                 return seg_tree_graph.view()
#             else:
#                 seg_tree_graph = graph_tree(tree_data, red=gold_sent_in_tree, grey=voc_in_tree)
#                 return seg_tree_graph.view()
#         else:
#             seg_tree_graph = graph_tree(tree_data)
#             return seg_tree_graph.view()
#     return result

# def bulk_segment(sents, voc, weight_function=rankOnly, sample_size=params.bulkSize, randomQ = False, graph_dataQ = True, supervisionQ = True, printQ=params.segment_printQ, parallelQ = True):
#     print('Segmenting...')
#     if randomQ:
#         sents_list = [sents[i] for i in random.sample(range(0, len(sents)), min(sample_size,len(sents)))]
#     else:
#         sents_list = sents[:sample_size]
    
#     start = time.perf_counter()

#     if parallelQ:
#         print("Method: Parallel")
#         forest_supervised = gfs.multiprocessing(functools.partial(segment_sent, voc=voc, weight_function=weight_function, printQ=printQ, graph_dataQ=graph_dataQ, supervisionQ=supervisionQ), enumerate(sents_list))
#     else:
#         forest_supervised = []
#         for i, sent in enumerate(sents_list):
#             if printQ:
#                 print((i,sent))
#             tree_supervised = segment_sent(sent, voc, weight_function=weight_function, graph_dataQ=graph_dataQ, supervisionQ=supervisionQ)
#             forest_supervised.append(tree_supervised)
    
#     finish = time.perf_counter()

#     if graph_dataQ and supervisionQ:
#         supervision_length = [s[-4] for s in forest_supervised]
#         supervision_chars_list = [s[-3] for s in forest_supervised]
#         supervision_words_list = [s[-2] for s in forest_supervised]
#     print('Done!')
#     print('\nSUMMARY:')
#     print(f"Forest of {len(forest_supervised)} trees computed in {round(finish-start,2)} secs")
#     if graph_dataQ and supervisionQ:
#         print(f"Average character Length: {round(sum(supervision_length)/len(supervision_length),2)} chars")
#         print(
#             f"Supervision Characters: {round(sum(supervision_chars_list)/len(supervision_chars_list),2)} % correct"
#         )
#         print(
#             f"Supervision Words: {round(sum(supervision_words_list)/len(supervision_words_list),2)} % correct\n"
#         )
#     return forest_supervised

# def graph_from_bulk(forest, n = 0, supervisionQ = True, viewQ = True):
#     seg_bulk_item = forest[n]
#     if len(seg_bulk_item) == 1 or not supervisionQ:
#         graph = graph_tree(seg_bulk_item[0])
#         return graph
#     elif len(seg_bulk_item)>1:
#         graph = graph_tree(seg_bulk_item[0], red=seg_bulk_item[1], grey=seg_bulk_item[2])
#         if viewQ:
#             graph.view()
#         return graph

# def supervision_rests(forest, voc):
#     rests = []
#     for tree in forest:
#         rests.extend(tree[-1])
#     rests = [(r, voc.get(r, 0)) for r in sorted(rests)]
#     print(f"Total terms non-identified: {len(rests)}")
#     print(f"Total terms out of Vocabulary: {len([r for r in rests if r[1] == 0])}")
#     print(
#         f"Out of voc / Non-id: {round(len([r for r in rests if r[1] == 0]) / len(rests),2)} %"
#     )
#     return rests


################################


# def save_ngrams(orthogonals, filename, freq_min = 0):
#     with open(f"{paths.orthogonals}/{filename}_{freq_min}.csv", "w") as nf:
#         for key, value in orthogonals.items():
#             if value >= freq_min:
#                 nf.write(f"{','.join(key)},{value}\n")



#################################



# # Measures

# # log_freq
# def log_freq_oR(contexts, orthogonals, term):
#     return [
#         log1p(orthogonals.get((term, context), 0)) + len(term) for context in contexts
#     ]


# def log_freq_oL(contexts, orthogonals, term):
#     return [
#         log1p(orthogonals.get((context, term), 0)) + len(term) for context in contexts
#     ]


# def log_freq_ooR(contexts, orthogonals, term):
#     return [
#         log1p(orthogonals.get((term, context), 0)) + len(context)
#         for context in contexts
#     ]


# def log_freq_ooL(contexts, orthogonals, term):
#     return [
#         log1p(orthogonals.get((context, term), 0)) + len(context)
#         for context in contexts
#     ]


# m_log_freq = [log_freq_oR, log_freq_oL, log_freq_ooR, log_freq_ooL]

# # binary
# def binary_oR(contexts, orthogonals, term):
#     return [
#         (1 if (term, context) in orthogonals else 0) for context in contexts
#     ]

# def binary_oL(contexts, orthogonals, term):
#     return [
#         (1 if (context, term) in orthogonals else 0) for context in contexts
#     ]

# m_binary = [binary_oR, binary_oL]

# # frequency

# def frequency_oR(contexts, orthogonals, term):
#     return [
#         orthogonals.get((term, context),0) for context in contexts
#     ]

# def frequency_oL(contexts, orthogonals, term):
#     return [
#         orthogonals.get((context, term),0) for context in contexts
#     ]

# def bigram_freq_R(contexts, orthogonals, term):
#     return [
#         orthogonals.get((*term,context),0) for context in contexts
#     ]

# def bigram_freq_L(contexts, orthogonals, term):
#     return [
#         orthogonals.get((context,*term),0) for context in contexts
#     ]


# m_frequency = [frequency_oR, frequency_oL]


# def build_freq_matrix(
#     terms:list,
#     contexts:list,
#     orthogonals:dict,
#     loadQ = False,
#     filename = "oR_freq_default",
#     trim=True,
#     saveQ = False,
#     measure=frequency_oR):
    
#     buildQ = "N"
#     owQ = "Y"

#     if loadQ:
#         if os.path.exists(paths.matrices+filename+".npz"):
#             oR_M = load_npz(paths.matrices+filename+".npz")
#             print(f"Frequency matrix loaded from:\n{paths.matrices}{filename}.npz")
#             if oR_M.shape != (len(terms),len(contexts)):
#                 if trim:
#                     oR_M = oR_M[:len(terms),:len(contexts)]
#                     print(f"Matrix trimmed to new shape: {oR_M.shape}")
#                 else:
#                     print("WARNING: Frequency matrix shape does not coincide with terms and contexts lengths!")
#             else:
#                 print("Shape verification: OK")
#         else:
#             buildQ = input(f"{paths.matrices}{filename}.npz does not exist. Do you want to build it? (Y/N, default=N)") or "N"
#             if buildQ == "N":
#                 return print("Frequency Matrix neither loaded nor built")

#     if not loadQ or buildQ == "Y":
#         print("Building oR Matrix...")
#         start = time.perf_counter()
#         oR_M = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure))
#         finish = time.perf_counter()
#         print(f"Frequency Matrix built in {round(finish-start,2)} secs.")

#         if saveQ:
#             print(f"Saving matrix as: {paths.matrices}{filename}.npz")
#             if os.path.exists(paths.matrices+filename+".npz"):
#                 owQ = input(f"File '{paths.matrices}{filename}.npz' already exists. Overwrite? (Y,N)") or "N"
#             if owQ == "N":
#                 print("Matrix not saved")
#             if owQ == "Y":
#                 save_npz(paths.matrices+filename+".npz", oR_M)
#                 print("Matrix saved!")
#     print("\n")
#     return oR_M

# def build_rho_matrix(
#     matrix,
#     thres = None,
#     loadQ = False, filename = "rho_default", saveQ = False):
#     buildQ = "N"
#     owQ = "Y"

#     if loadQ:
#         if os.path.exists(paths.matrices+filename+".npz"):
#             oR_M = load_npz(paths.matrices+filename+".npz")
#             print(f"rho matrix loaded from:\n{paths.matrices}{filename}.npz")
#         else:
#             buildQ = input(f"{paths.matrices}{filename}.npz does not exist. Do you want to build it? (Y/N, default=N)") or "N"
#             if buildQ == "N":
#                 return print("rho Matrix neither loaded nor built")

#     if not loadQ or buildQ == "Y":
#         print("Building rho Matrix...")
#         start = time.perf_counter()
#         rho_M = matrix.dot(matrix.T)
#         finish = time.perf_counter()
#         print(f"Frequency Matrix built in {round(finish-start,2)} secs.")

#         if thres != None:
#             print("Applying threshold...")
#             start = time.perf_counter()
#             rho_M.data = np.where(rho_M.data < thres, 0, rho_M.data)
#             print("Eliminating zeros...")
#             rho_M.eliminate_zeros()
#             finish = time.perf_counter()
#             print(f"Frequency Matrix built in {round(finish-start,2)} secs.")

#         if saveQ:
#             print(f"Saving matrix as: {paths.matrices}{filename}.npz")
#             if os.path.exists(paths.matrices+filename+".npz"):
#                 owQ = input(f"File '{paths.matrices}{filename}.npz' already exists. Overwrite? (Y,N)") or "N"
#             if owQ == "N":
#                 print("Matrix not saved")
#             if owQ == "Y":
#                 save_npz(paths.matrices+filename+".npz", rho_M)
#                 print("Matrix saved!")
#     print("\n")
#     return rho_M

# def build_matrices(terms, contexts, orthogonals, measure = params.measure):

#     print(f"Building Matrices...")
#     print(f"Type of matrices: {measure}\n")

#     contexts = list(contexts)
#     terms = list(terms)
#     measure_oR = eval(measure)[0]
#     measure_oL = eval(measure)[1]
#     if len(eval(measure)) == 4:
#         measure_ooR = eval(measure)[2]
#         measure_ooL = eval(measure)[3]

#     print("Building oR Matrix...")
#     start = time.perf_counter()
#     oR_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_oR))
#     finish = time.perf_counter()
#     print(f"oR Matrix built in {round(finish-start,2)} secs.\n")

#     print("Building oL Matrix...")
#     start = time.perf_counter()
#     oL_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_oL))
#     finish = time.perf_counter()
#     print(f"oL Matrix built in {round(finish-start,2)} secs.\n")

#     print("Building ooR and ooL Matrices...")

#     if len(eval(measure)) == 2:
#         ooR_Matrix = oR_Matrix.T
#         ooL_Matrix = oL_Matrix.T
#         print("ooR and ooL built as Transpose from oR and oL.\n")
#     elif len(eval(measure)) == 4:
#         print("Building ooL Matrix...")
#         start = time.perf_counter()
#         ooR_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_ooR)).T
#         finish = time.perf_counter()
#         print(f"oR Matrix built in {round(finish-start,2)} secs.\n")
#         print("Building ooL Matrix...")
#         start = time.perf_counter()
#         ooL_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_ooL)).T
#         finish = time.perf_counter()
#         print(f"oL Matrix built in {round(finish-start,2)} secs.\n")
#     return [oR_Matrix, oL_Matrix, ooR_Matrix, ooL_Matrix]

# def build_matrices_R(terms, contexts, orthogonals, measure = params.measure):

#     print(f"Building Matrices...")
#     print(f"Type of matrices: {measure}\n")

#     contexts = list(contexts)
#     terms = list(terms)
#     measure_oR = eval(measure)[0]
#     # measure_oL = eval(measure)[1]
#     if len(eval(measure)) == 4:
#         measure_ooR = eval(measure)[2]
#         # measure_ooL = eval(measure)[3]

#     print("Building oR Matrix...")
#     start = time.perf_counter()
#     oR_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_oR))
#     finish = time.perf_counter()
#     print(f"oR Matrix built in {round(finish-start,2)} secs.\n")

#     # print("Building oL Matrix...")
#     # start = time.perf_counter()
#     # oL_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_oL))
#     # finish = time.perf_counter()
#     # print(f"oL Matrix built in {round(finish-start,2)} secs.\n")

#     print("Building ooR and ooL Matrices...")

#     if len(eval(measure)) == 2:
#         ooR_Matrix = oR_Matrix.T
#         # ooL_Matrix = oL_Matrix.T
#         print("ooR built as Transpose from oR.\n")
#     elif len(eval(measure)) == 4:
#         print("Building ooR Matrix...")
#         start = time.perf_counter()
#         ooR_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_ooR)).T
#         finish = time.perf_counter()
#         print(f"ooR Matrix built in {round(finish-start,2)} secs.\n")
#         # print("Building ooL Matrix...")
#         # start = time.perf_counter()
#         # ooL_Matrix = csr_matrix(matrix_maker(terms, contexts, orthogonals, measure_ooL)).T
#         # finish = time.perf_counter()
#         # print(f"ooL Matrix built in {round(finish-start,2)} secs.\n")
#     return [oR_Matrix, ooR_Matrix]

# def build_pmi_matrix(freq_matrix, alpha=.75, type="sppmi", loadQ = False, filename = "oR_pmi_default", trim=True, saveQ = False):
#     buildQ = "N"
#     owQ = "Y"

#     if loadQ:
#         if os.path.exists(paths.matrices+filename+".npz"):
#             pmi_matrix = load_npz(paths.matrices+filename+".npz")
#             print(f"PMI matrix loaded from:\n{paths.matrices}{filename}.npz")
#             if pmi_matrix.shape != freq_matrix.shape:
#                 if trim:
#                     pmi_matrix = pmi_matrix[:freq_matrix.shape[0],:freq_matrix.shape[1]]
#                     print(f"Matrix trimmed to new shape: {pmi_matrix.shape}")
#                 else:
#                     print("WARNING: PMI matrix shape does not coincide with terms and contexts lengths!")
#             else:
#                 print("Shape verification: OK")
#         else:
#             buildQ = input(f"{paths.matrices}{filename}.npz does not exist. Do you want to compute it? (Y/N, default=N)") or "N"
#             if buildQ == "N":
#                 return print("PMI Matrix neither loaded nor computed")

#     if not loadQ or buildQ == "Y":
#         print("Computing PMI Matrix...")
#         print(f"Type: {type}")
#         if "s" in type:
#             print(f"Smoothing (alpha): {alpha}")
#         start = time.perf_counter()
#         pmi_matrix = gfs.pmi(freq_matrix,alpha=alpha,type_pmi=type)
#         finish = time.perf_counter()
#         print(f"PMI Matrix built in {round(finish-start,2)} secs.")

#         if saveQ:
#             print(f"Saving matrix as: {paths.matrices}{filename}.npz")
#             if os.path.exists(paths.matrices+filename+".npz"):
#                 owQ = input(f"File '{paths.matrices}{filename}.npz' already exists. Overwrite? (Y,N)") or "N"
#             if owQ == "N":
#                 print("Matrix not saved")
#             if owQ == "Y":
#                 if type in {"spmi","sppmi","ssppmi"}:
#                     alpha = f"_{alpha}"
#                 else:
#                     alpha = ""
#                 # def_fn = f"{dataset}_oR_{type}{alpha}_{pmi_matrix.shape[0]}-{pmi_matrix.shape[1]}"
#                 # filename = input(f"File name? Default: {def_fn}") or def_fn
#                 save_npz(paths.matrices+filename+".npz", pmi_matrix)
#                 print("Matrix saved!")
#     print("\n")
#     return pmi_matrix

# def build_bo_types_from_eig(terms, contexts, v_t, v_c, size=100,agg=.25,thres=.01,top=None, separate_p_n=True):

#     def extract_prob_units(vector,units,p_n_filter=None):
#         if p_n_filter == "p":
#             op = min
#         elif p_n_filter == "n":
#             op = max
#         else:
#             op = lambda x,y:x

#         eig_items = sorted([(v**2,k) for v,k in zip(vector,units) if op(0,v)==0], reverse=True)
#         return eig_items

#     def cut(eig_items):
#         if top == None:
#             eig_items_cut = []
#             if len(eig_items)>0:
#                 s = 0
#                 t = 0
#                 while s <=agg and eig_items[t][0] > thres:
#                     eig_items_cut.append(eig_items[t][1])
#                     s += eig_items[t][0]
#                     t += 1
#                     if t>len(eig_items):
#                         break
#         else:
#             eig_items_cut = [i for v,i in eig_items[:top]]
#         return tuple(sorted(eig_items_cut))

#     bo_Types = []

#     for eig_n in range(min(len(v_t),len(v_c),size)):
#         if separate_p_n:
#             eig_terms_p = extract_prob_units(v_t[eig_n],terms,p_n_filter="p")
#             eig_contexts_p = extract_prob_units(v_c[eig_n],contexts,p_n_filter="p")
#             eig_terms_n = extract_prob_units(v_t[eig_n],terms,p_n_filter="n")
#             eig_contexts_n = extract_prob_units(v_c[eig_n],contexts,p_n_filter="n")

#             bo_Types.append((cut(eig_terms_p),cut(eig_contexts_p)))
#             bo_Types.append((cut(eig_terms_n),cut(eig_contexts_n)))
#         else:
#             eig_terms = extract_prob_units(v_t[eig_n],terms)
#             eig_contexts = extract_prob_units(v_c[eig_n],contexts)

#             bo_Types.append((cut(eig_terms),cut(eig_contexts)))

#     return bo_Types

# def cut_thres(row, thres):
#     ortho_index = (row > thres) * 1
#     return ortho_index

# def cut_mean(row, thres):
#     if len(row.data) == 0:
#         ortho_index = row
#     else:
#         mean_cut = row.data.mean()
#         if mean_cut == row.data.max():
#             mean_cut = mean_cut / 2
#         mean_cut = row.data.mean()*thres
#         ortho_index = (row > mean_cut) * 1
#     return ortho_index





# def unique_rows_fast(matrix, indexQ = False):
#     random_factor = np.random.rand(matrix.shape[1])
#     unicity_test = matrix.dot(random_factor)
#     groups = defaultdict(list)
#     for v,k in enumerate(unicity_test):
#         groups[k].append(v)
#     row_groups = list(groups.values())
#     index_unique = [i[0] for i in row_groups]
#     if indexQ:
#         return (matrix[index_unique], row_groups)
#     else:
#         return matrix[index_unique]

# def unique_rows(matrix, indexQ = False):
#     groups = defaultdict(list)
#     for v,k in enumerate(matrix):
#         groups[str(k)].append(v)
#     row_groups = list(groups.values())
#     index_unique = [i[0] for i in row_groups]
#     if indexQ:
#         return (matrix[index_unique], row_groups)
#     else:
#         return matrix[index_unique]

# def compress_matrix(matrix, indexQ = False, fast=False, columns=True, verbose = True):
#     """
#     Takes a matrix (preferably in csr mode, in particular if fast=False) and yields a matrix with unique rows (and columns if columns=True). In other words, it eliminates duplicate rows (and columns). In addition it provides a list of the groups of rows (and columns) that are identical in order to recover the original indexes when needed 
#     """
#     if verbose:
#         print(f"Compressing matrix. Fast: {fast}. Columns: {columns}")
#     start = time.perf_counter()
#     if indexQ:
#         if fast:
#             matrix_unique, unique2bin_term_index = unique_rows_fast(matrix, indexQ=True)
#             if columns==False:
#                 result = (matrix_unique, unique2bin_term_index)
#             else:
#                 matrix_unique, unique2bin_context_index = unique_rows_fast(matrix_unique.T, indexQ=True)
#                 matrix_unique = matrix_unique.T
#                 result = (matrix_unique, unique2bin_term_index, unique2bin_context_index)
#         else:
#             matrix_unique, unique2bin_term_index = unique_rows(matrix, indexQ=True)
#             if columns==False:
#                 result = (matrix_unique, unique2bin_term_index)
#             else:
#                 matrix_unique, unique2bin_context_index = unique_rows(matrix_unique.T, indexQ=True)
#                 matrix_unique = matrix_unique.T
#                 result = (matrix_unique, unique2bin_term_index, unique2bin_context_index)
#     else:
#         if fast:
#             matrix_unique = unique_rows_fast(matrix, indexQ=False)
#             if columns==True:
#                 matrix_unique = unique_rows_fast(matrix_unique.T, indexQ=False)
#                 matrix_unique = matrix_unique.T
#             # result = matrix_unique
#         else:
#             matrix_unique = unique_rows(matrix, indexQ=False)
#             if columns==True:
#                 matrix_unique = unique_rows(matrix_unique.T, indexQ=False)
#                 matrix_unique = matrix_unique.T
#         result = matrix_unique
#     finish = time.perf_counter()
#     if verbose:
#         print(f"Compressed matrix computed in {round(finish-start,2)} secs.")
#         print(f"New matrix size: {matrix_unique.shape}\n")
#     return result 


# def compute_ps_row(truncate, n_subset_M,row):
#     ps_row = gfs.sp_unique(truncate_M(csr_matrix.multiply(row,n_subset_M), min_len = truncate))
#     return ps_row


# def tred_graph(Tt_dict):
#     """
#     Build edge list of transitive reduction graph out of a Type-term dicitonnary or a list of Types
#     """
#     if type(Tt_dict) == dict:
#         type_list = list(Tt_dict.keys())
#     else:
#         type_list = Tt_dict
#     lattice_edges = subtype_rels(type_list, raw = False)
#     graph_nx = nx.DiGraph()
#     graph_nx.add_edges_from(lattice_edges)#, edge_type='sub_type')
#     graph_nx.add_edges_from([((("⊤",),i[-2],i[-1]), i) for i in type_list])#, edge_type='sub_type')
#     graph_nx.add_edges_from([(i, (("⊥",),i[-2],i[-1])) for i in type_list])#, edge_type='sub_type')
#     graph_nx_reduction = nx.algorithms.dag.transitive_reduction(graph_nx)
#     return graph_nx_reduction


# def sent_type_graph(
#     complete_nx_graph,
#     inter=True,
#     interonly=False,
#     fname="Graph",
#     colors_list=[
#         "burlywood2",
#         "blanchedalmond",
#         "grey80",
#         "LightSteelBlue",
#     ],
#     dependenciesQ=True,
#     LRQ = True,
#     font="RobotoMono-Thin"
# ):
#     print("Building plot of graph...")
#     LR_intersect = set([n[0] for n in complete_nx_graph.nodes() if n[-1] == 'L']) & set([n[0] for n in complete_nx_graph.nodes() if n[-1] == 'R'])

#     if inter:
#         complete_nx_graph = nx.relabel_nodes(complete_nx_graph, lambda x: (x[0],'t','LR') if x[0] in LR_intersect else x)
#     if inter and interonly:
#         # nodes_to_exclude = [n for n in complete_nx_graph.nodes() if n[-1] != "LR"]
#         complete_nx_graph.remove_nodes_from([n for n in complete_nx_graph.nodes() if n[-1] != "LR"])
#         # complete_nx_graph = complete_nx_graph_pre



#     color_switch = {
#         ("o","L") : 1,
#         ("bo","R") : 0,
#         ("bo","L") : 0,
#         ("o","R") : 1,
#         ("obo","L") : 2,
#         ("obo","R") : 2,
#         ("t","LR") : 3,
#     }

#     Complete_LR = gv.Digraph(name=fname, strict=True)

#     # top_bottom = {("⊤",), ("⊥",)}
#     Complete_LR.attr("node", fontname = font)
#     Complete_LR.attr("edge", color="grey80")

#     Complete_LR.edges(
#         [(str(i), str(j)) for i, j, d in complete_nx_graph.edges(data=True) if d == {}]
#     )

#     for node in complete_nx_graph.nodes:
#         # if node[1] == "o":
#         Complete_LR.node(
#             str(node),
#             make_label(node[0], LRN=node[2]),
#             style="filled",
#             color=colors_list[color_switch[node[1:3]]],
#             shape="doubleoctagon" if node[0] in LR_intersect else "cds",
#             orientation="0" if node[-1] == "L" else "90",
#             margin="0,0" if node[0] in LR_intersect else "0.2,0.2",
#         )

#     if dependenciesQ:
#         Complete_LR.attr("edge",
#         #  arrowhead="none",
#           color="darksalmon")
#         Complete_LR.edges(
#             [
#                 (str(i), str(j))
#                 for i, j, d in complete_nx_graph.edges(data=True)
#                 if d != {}
#             ]
#         )
#     print("Finished!")
#     return Complete_LR
# %%
